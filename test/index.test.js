var supertest = require('supertest')

describe("Test Index", () => {

    var server
    beforeEach(() => {
        server = require('../app')
    })

    afterEach(() => {
        // close server
    })

    it('Should return 200', async() => {
        var res = await supertest(server).get('/')

        expect(res.status).toBe(200)
    })
})