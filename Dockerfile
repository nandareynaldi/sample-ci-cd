FROM node:14-alpine

# Set Workdir
WORKDIR /app

# Copy Package Json
COPY package*.json .

# Install Dependency
RUN npm install

# Set dir permission for /app directory
COPY --chown=node:node . .

# Define user 
USER node

# Expose and bind port
EXPOSE 3000:3000

CMD ["npm", "start"]
